# Exercice 1

## Question 1

$ openssl genrsa -des3 1024 > maCle.pem
$ openssl rsa -des3 -in maCle.pem -pubout > maCle.pem.pub

pass: coucou

$ openssl req -config req.cnf -key maCle.pem -new -out maRequete.pem

## Question 2

$ openssl req -config req.cnf -key maCle.pem -new -text -out maRequete.txt

# Exercice 2

## Question 1

Creation de l'authorité de certification
$ openssl req -config req.cnf -new -x509 -keyout terra.key -out terra.cert -days 365

## Question 2

openssl x509 -in pereUbuCertif_old.pem -text > pereUbuCertif_old.pem.text

Le certificat n'est plus valide depuis 2012
Non on ne peut pas le renouveller un, il faut en recreer un avec les memes infos

# Exercice 3

## Question 1

