# Exercice 1: RSA

## Question 1

### generation clef privee

$ openssl genrsa -out csgo.key
Generating RSA private key, 2048 bit long modulus
..........+++
..................................................................+++
e is 65537 (0x010001)

### Generation clef publique

oktomus at pancake in ~/Cours/SSR/TP3 (master●)
$ openssl rsa -in csgo.key -pubout > csgo.key.pub
writing RSA key

### Affichage clef

$ openssl rsa -in uneClePublique.pem -pubin -modulus -text
Public-Key: (2048 bit)
Modulus:
    00:ef:c2:9d:36:22:c6:3a:e8:5e:fa:03:d4:11:dd:
    ee:b3:18:c9:61:7c:10:02:be:93:aa:25:87:38:23:
    36:a3:0f:b9:41:27:af:e8:e0:72:66:48:c1:af:e1:
    c7:b7:8f:47:f2:5c:97:0a:92:4a:4f:b3:1f:d7:30:
    c8:2e:4f:96:2c:c8:d8:67:21:45:c4:8e:84:f9:21:
    ff:95:22:a3:9a:8f:f2:ba:d8:6a:72:dc:b7:11:a3:
    92:8f:20:c7:b8:9c:15:da:0e:f7:d5:cc:d6:57:1f:
    1b:fa:aa:17:c7:16:25:53:3a:6c:b9:97:d6:10:8b:
    d2:d4:4f:56:29:6b:54:ff:cb:83:6e:e2:89:c4:86:
    81:9e:88:96:fa:58:71:32:ea:23:89:58:d8:cd:b9:
    33:61:4f:25:9f:82:8d:6f:97:c6:94:c8:5c:09:98:
    7f:2c:4f:0f:c7:cf:98:96:f8:47:fb:1a:5f:a6:dd:
    70:7f:3d:bb:f6:f7:5b:ed:52:64:25:d6:95:f3:55:
    c5:49:be:6e:0f:d2:85:50:6a:ca:92:2b:57:ef:8f:
    7c:bf:e2:03:7f:7e:72:1d:dd:03:da:3d:47:41:9b:
    bc:05:2e:8b:9f:55:3e:8f:27:89:af:c4:97:99:46:
    ee:e5:86:6f:c4:03:fb:58:8c:7a:ca:09:ef:f1:81:
    63:49
Exponent: 65537 (0x10001)
Modulus=EFC29D3622C63AE85EFA03D411DDEEB318C9617C1002BE93AA2587382336A30FB94127AFE8E0726648C1AFE1C7B78F47F25C970A924A4FB31FD730C82E4F962CC8D8672145C48E84F921FF9522A39A8FF2BAD86A72DCB711A3928F20C7B89C15DA0EF7D5CCD6571F1BFAAA17C71625533A6CB997D6108BD2D44F56296B54FFCB836EE289C486819E8896FA587132EA238958D8CDB933614F259F828D6F97C694C85C09987F2C4F0FC7CF9896F847FB1A5FA6DD707F3DBBF6F75BED526425D695F355C549BE6E0FD285506ACA922B57EF8F7CBFE2037F7E721DDD03DA3D47419BBC052E8B9F553E8F2789AFC4979946EEE5866FC403FB588C7ACA09EFF1816349
writing RSA key
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA78KdNiLGOuhe+gPUEd3u
sxjJYXwQAr6TqiWHOCM2ow+5QSev6OByZkjBr+HHt49H8lyXCpJKT7Mf1zDILk+W
LMjYZyFFxI6E+SH/lSKjmo/yuthqcty3EaOSjyDHuJwV2g731czWVx8b+qoXxxYl
UzpsuZfWEIvS1E9WKWtU/8uDbuKJxIaBnoiW+lhxMuojiVjYzbkzYU8ln4KNb5fG
lMhcCZh/LE8Px8+YlvhH+xpfpt1wfz279vdb7VJkJdaV81XFSb5uD9KFUGrKkitX
7498v+IDf35yHd0D2j1HQZu8BS6Ln1U+jyeJr8SXmUbu5YZvxAP7WIx6ygnv8YFj
SQIDAQAB
-----END PUBLIC KEY-----

## Question 2

**Chiffre un message clair avec la clef pub de thibaut**
$ openssl rsautl -in short_text.kevin.clair -inkey pikachu.key -pubin -encrypt -out short_text.kevin.crypt

**Dechiffrer le message de thibaut avce ma clef privee**
$ openssl rsautl -in short_text.thibpourkevin.chiffr -inkey csgo.key -out short_text.thibpourkevin.clair -decrypt

## Question 3

$ base64 --decode last_passwd.64 > last_passwd                                             
$ openssl rsautl -in passwd.rsa -inkey key_for_passwd.pem -decrypt -out mdp.blowfish -passin file:last_passwd
$ vim mdp.blowfish
vendredi1002
$ openssl enc -d -bf-cbc -in encrypted.cbc -out text.cbc.clair                                                                         

## Question 4

# Exercice 2

## Question 1

# Exerice 3

## Question 1

$ ps -aux | grep ssh-agent
ssh-agent

SSh agent fonctionne avec un socket

## Question 
